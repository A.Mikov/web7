$(document).ready(function() {
    $(".newslider").slick({
        infinite: true,
        dots: true,
        dotsClass: "s_dots",
        prevArrow: $("#prev"),
        nextArrow: $("#next"),
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }]
    });
});
